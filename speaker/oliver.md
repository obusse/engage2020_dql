# Speaker info Oliver

Oliver Busse is working as a Senior ICS Consultant & Software Architect for the We4IT Group in Dresden, Germany. He is a experienced developer since 2000 in the IBM Notes and Domino environment and also known speaker at conferences (DNUG, OSA, ICS.UG, Engage.UG, ISBG, ICON.UK, SUTOL). Oliver publishes regularly on his blog http://oliverbusse.com.
He was an IBM Champion for ICS from 2015 to 2018 and is an HCL Master in 2020.
