/**
 * 
 */
package org.engage2020.controller;

import java.io.Serializable;

import org.engage2020.helper.ConfigHelper;

import com.ibm.xsp.extlib.util.ExtLibUtil;

import lotus.domino.Database;
import lotus.domino.Document;

/**
 * @author Oliver Busse, oliverbusse.com
 * @created Feb 10, 2020 Licensed under MIT license
 *
 */
public class SetupPageController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String server;
	private String filePath;

	public SetupPageController() {
		try {
			Document doc = ConfigHelper.getSetupDocument();
			if (doc != null) {
				Database db = ExtLibUtil.getCurrentDatabase();
				String itServer = doc.getItemValueString("server");
				String itFilePath = doc.getItemValueString("filePath");
				if (itFilePath.isEmpty()) {
					itFilePath = db.getFilePath();
					itFilePath = itFilePath.replaceAll("\\\\", "/");
					itFilePath = itFilePath.indexOf("/") != -1 ? itFilePath.substring(0, itFilePath.lastIndexOf("/"))
							: itFilePath;
					itFilePath = itFilePath;
				}

				setServer(itServer.isEmpty() ? db.getServer() : itServer);
				setFilePath(itFilePath);
			} else {
				System.out.println("No config found");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void save() {
		try {
			Document doc = ConfigHelper.getSetupDocument();
			if (doc != null) {
				doc.replaceItemValue("form", "config");
				doc.replaceItemValue("server", this.server);
				doc.replaceItemValue("filePath", this.filePath);
				doc.save();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
