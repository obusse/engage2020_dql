/**
 * 
 */
package org.engage2020.helper;

import com.ibm.xsp.extlib.util.ExtLibUtil;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NoteCollection;
import lotus.domino.NotesException;

/**
 * @author Oliver Busse, oliverbusse.com
 * @created Feb 10, 2020 Licensed under MIT license
 *
 */
public class ConfigHelper {
	
	private String server;
	private String filePath;
	
	public ConfigHelper() {
		try {
			Document config = ConfigHelper.getSetupDocument();
			server = config.getItemValueString("server");
			filePath = config.getItemValueString("filePath");
		} catch (NotesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Document getSetupDocument() {
		try {
			Database db = ExtLibUtil.getCurrentDatabase();
			NoteCollection col = db.createNoteCollection(false);
			col.setSelectDocuments(true);
			col.setSelectionFormula("form=\"config\"");
			col.buildCollection();
			if (col.getCount() == 0) {
				return db.createDocument();
			} else {
				return db.getDocumentByID(col.getFirstNoteID());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	/**
	 * @throws NotesException
	 */
	public Database getSourceDatabase() {
		try {
			Database db = ExtLibUtil.getCurrentSession().getDatabase(server, filePath);
			return db;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public String getServer() {
		return server;
	}
	
	public String getFilePath() {
		return filePath;
	}

	
}
