package org.engage2020.dql;

import java.util.EnumSet;
import java.util.List;

import org.engage2020.Utils;
import org.engage2020.helper.ConfigHelper;

import com.ibm.commons.util.StringUtil;
import com.ibm.commons.util.io.json.JsonJavaArray;
import com.ibm.commons.util.io.json.JsonJavaObject;
import com.ibm.xsp.extlib.util.ExtLibUtil;
import com.mindoo.domino.jna.NotesCollection;
import com.mindoo.domino.jna.NotesCollection.Direction;
import com.mindoo.domino.jna.NotesDatabase;
import com.mindoo.domino.jna.NotesDatabase.HarvestMode;
import com.mindoo.domino.jna.NotesDbQueryResult;
import com.mindoo.domino.jna.NotesViewEntryData;
import com.mindoo.domino.jna.constants.DBQuery;
import com.mindoo.domino.jna.constants.Navigate;
import com.mindoo.domino.jna.constants.ReadMask;
import com.mindoo.domino.jna.dql.DQL;
import com.mindoo.domino.jna.dql.DQL.DQLTerm;

public class JnaDqlSearch {

	public static void performSearch(JsonJavaObject out, String searchValue, 
			String sortBy, boolean sortAscending, int offset, int pageSize) {
		
		Long started = System.currentTimeMillis();

		// open target db
		ConfigHelper config = new ConfigHelper();
		NotesDatabase db = new NotesDatabase(ExtLibUtil.getCurrentSession(), config.getServer(), config.getFilePath());
		db.harvestDesign(HarvestMode.UPDATE);

		DQLTerm dqlQuery = DQL.or(
				DQL.item("firstname").contains(searchValue + "*"),
				DQL.item("lastname").contains(searchValue + "*"),
				DQL.item("street").contains(searchValue + "*"),
				DQL.item("email").contains(searchValue + "*"),
				DQL.item("city").contains(searchValue + "*"));

		// execute DQL search
		NotesDbQueryResult queryResult;
		
		// run query without returning EXPLAIN text, use defaults for limits
		// queryResult = db.query(dqlQuery);

		// run query with EXPLAIN text, use defaults for limits
		queryResult = db.query(dqlQuery, EnumSet.of(DBQuery.EXPLAIN));
		
		Utils.log("query executed in " + (System.currentTimeMillis() - started) + "ms");
		started = System.currentTimeMillis();

		// open view with sortable columns Lastname/Firstname
		NotesCollection peopleView = db.openCollectionByName("contacts");
		
		// set result sorting (column must be sortable via click)
		if (StringUtil.isEmpty(sortBy)) {
			sortBy = "lastname";
		}
		peopleView.resortView(sortBy, sortAscending ? Direction.Ascending : Direction.Descending);
		
		Utils.log("resorted in " + (System.currentTimeMillis() - started) + "ms");
		started = System.currentTimeMillis();

		// change view selection to our DQL result
		boolean clearPrevSelection = true;
		peopleView.select(queryResult.getIDTable(), clearPrevSelection);

		Utils.log("results applied in " + (System.currentTimeMillis() - started) + "ms");
		started = System.currentTimeMillis();

		// read all selected view entries
		List<NotesViewEntryData> entries = peopleView.getAllEntries(
				"0",
				1 + offset,
				EnumSet.of(Navigate.NEXT_SELECTED),
				pageSize,
				EnumSet.of(ReadMask.NOTEID, ReadMask.SUMMARYVALUES),
				new NotesCollection.EntriesAsListCallback(pageSize));

		Utils.log(entries.size() + " view rows read in " + (System.currentTimeMillis() - started) + "ms");
		
		started = System.currentTimeMillis();

		/*
		 * Build an array that can be send back to the REST client
		 *
		 * Note that this is not needed if you override the callback function (last parameter
		 * in the getAllEntries() call above), which is the recommended approach
		 * 
		 * We're currently using the default EntriesAsListCallback for demo purposes
		 */
		JsonJavaArray arr = new JsonJavaArray();

		for (int i = 0; i < entries.size(); i++) {
			NotesViewEntryData currEntry = entries.get(i);
			JsonJavaObject jo = new JsonJavaObject();
			jo.put("docid", currEntry.getNoteId());
			jo.put("lastname", currEntry.get("lastName"));
			jo.put("firstname", currEntry.get("firstName"));
			jo.put("street", currEntry.get("street"));
			jo.put("city", currEntry.get("city"));
			jo.put("email", currEntry.get("email"));

			arr.add(jo);
		}

		out.put("data", arr);
		out.put("explain", queryResult.getExplainText());
		out.put("count", queryResult.getIDTable().toArray().length);
		out.put("query", dqlQuery.toString());
		
		Utils.log(arr.size() + " entries read in " + (System.currentTimeMillis() - started) + "ms");

	}
}
