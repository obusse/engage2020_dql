package org.engage2020.dql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ibm.xsp.extlib.util.ExtLibUtil;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.DominoQuery;
import lotus.domino.NotesException;

public class DQLTester implements Serializable {

	private static String fakenamePath = "engage/fakenames.nsf";

	private String query;
	private String explain;
	
	private List<HashMap<String, String>> searchResults = new ArrayList<HashMap<String, String>>();

	public DQLTester() {
		// default query
		this.query = "city contains ('Arnhem', 'Amsterdam' )";
	}

	public void searchExplain() {
		try {
			
			DominoQuery domQuery = getDomQuery();

			this.explain = domQuery.explain(this.query);
			this.search(domQuery);

		} catch (NotesException e) {
			e.printStackTrace();
		}

	}

	private void search(DominoQuery domQuery) throws NotesException {
		DocumentCollection res = domQuery.execute(this.query);

		this.searchResults.clear();

		Document doc = res.getFirstDocument();
		while (null != doc) {

			HashMap<String, String> result = new HashMap<String, String>();
			result.put("firstName", doc.getItemValueString("firstName"));
			result.put("lastName", doc.getItemValueString("lastName"));
			result.put("city", doc.getItemValueString("city"));
			result.put("email", doc.getItemValueString("email"));

			searchResults.add(result);

			Document next = res.getNextDocument();
			doc.recycle();
			doc = next;
		}

	}

	private static DominoQuery getDomQuery() throws NotesException {
		Database dbFake = ExtLibUtil.getCurrentSession().getDatabase("", fakenamePath);
		DominoQuery domQuery = dbFake.createDominoQuery();
		return domQuery;
	}

	public List<HashMap<String, String>> getSearchResults() {
		return searchResults;
	}

	// read the current indexes
	public void listIndex() throws NotesException {
		this.searchResults.clear();
		this.explain = getDomQuery().listIndexes();
	}

	// update the DQL indexes
	public void updateIndex() throws NotesException {
		this.searchResults.clear();
		getDomQuery().setRebuildDesignCatalog(true);
		explain = "Done updating indexes on database " + fakenamePath;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getExplain() {
		return explain;
	}

	public int getNumResults() {
		return this.searchResults.size();
	}

}
