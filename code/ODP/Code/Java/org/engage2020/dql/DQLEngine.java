/**
 * 
 */
package org.engage2020.dql;

import java.util.HashMap;
import java.util.Map;

import org.engage2020.helper.ConfigHelper;

import com.ibm.xsp.extlib.util.ExtLibUtil;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.DominoQuery;
import lotus.domino.NotesException;

/**
 * @author Oliver Busse, oliverbusse.com
 * @created Jan 27, 2020 Licensed under MIT license
 *
 */
public class DQLEngine {
	
	private String server;
	private String filePath;
	
	public DQLEngine() {
		try {
			Document config = ConfigHelper.getSetupDocument();
			server = config.getItemValueString("server");
			filePath = config.getItemValueString("filePath");
		} catch (NotesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Map<String, Object> executeSearch(String query) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			DocumentCollection result = null;
			Database db = getSourceDatabase();
			
			// create the DQL object
			DominoQuery dQuery = db.createDominoQuery();
			
			// set a timeout on the query
			dQuery.setTimeoutSec(2);
			
			// execute the query
			result = dQuery.execute(query);
			
			map.put("result", result);
			map.put("count", result.getCount());
			
			// explain is a special method that returns a string explaining how the search was performed
			map.put("explain", dQuery.explain(query));
			map.put("query", query);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		// there are several values returned so we use a map to provide them at once
		return map;
	}

	/**
	 * @throws NotesException
	 */
	public Database getSourceDatabase() {
		try {
			Database db = ExtLibUtil.getCurrentSession().getDatabase(server, filePath);
			return db;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public String getServer() {
		return server;
	}
	
	public String getFilePath() {
		return filePath;
	}

}
