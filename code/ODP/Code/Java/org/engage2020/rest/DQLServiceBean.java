package org.engage2020.rest;

import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.engage2020.Utils;
import org.engage2020.dql.DQLEngine;

import com.ibm.commons.util.io.json.JsonJavaArray;
import com.ibm.commons.util.io.json.JsonJavaObject;
import com.ibm.domino.services.ServiceException;
import com.ibm.domino.services.rest.RestServiceEngine;
import com.ibm.xsp.extlib.component.rest.CustomService;
import com.ibm.xsp.extlib.component.rest.CustomServiceBean;

import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.NotesException;

/**
 * 
 * @author Oliver Busse, oliverbusse.com
 * @created Jan 26, 2020 Licensed under MIT license
 *
 */
public class DQLServiceBean extends CustomServiceBean {
	
	private static String queryTemplateFieldFT = "lastname contains (%s) or firstname contains (%s)";
	private static String queryTemplateDocFT = "contains (%s)";
	
	private HttpServletRequest request;
	
	@Override
	/**
	 * this renders the response for the JSON rest service
	 */
	public void renderService(CustomService service, RestServiceEngine engine) throws ServiceException {
		request = engine.getHttpRequest();
		HttpServletResponse response = engine.getHttpResponse();
		response.setHeader("Content-Type", "application/json; charset=UTF-8");
		PrintWriter writer = null;
		
		long start = System.currentTimeMillis();
		
		try {
			
			//add CORS headers
			response.addHeader("Access-Control-Allow-Origin", "*");
			
			// grab the parameters
			String searchValue = request.getParameter("query");
			
			// prevent finding names with spaces
			if (searchValue.isEmpty()) {
				searchValue = "_dummy_";
			}
			
			writer = response.getWriter();
			JsonJavaObject out = renderDefault();
			
			// we differentiate some actions
			String pathInfo = request.getPathInfo();
			
			out.put("search", searchValue);
			out.put("context", pathInfo.substring(1));
			out.put("status", "ok");
			
			if (pathInfo.equalsIgnoreCase("/view")) {
				// everything view related
				out = renderView();
			} else if (pathInfo.equalsIgnoreCase("/doc")) {
				// doc related
				out = renderDoc();
			} else if (pathInfo.equalsIgnoreCase("/search")) {
				// perform a normal search for lastname and firstname
				performSearch(out, searchValue);
			}
			
			writer.write(out.toString());
			
			Utils.log("Done in " + (System.currentTimeMillis() - start) + "ms");
			
		} catch (Exception e) {
			if (writer != null) {
				JsonJavaObject err = new JsonJavaObject();
				err.put("status", "error");
				err.put("message", e.getMessage());
				writer.write(err.toString());
				writer.close();
			}
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	/**
	 * default empty response
	 * @return
	 */
	private JsonJavaObject renderDefault() {
		JsonJavaObject jo = new JsonJavaObject();
		return jo;
	}

	/**
	 * default empty response
	 * @return
	 */
	private JsonJavaObject renderView() {
		JsonJavaObject jo = new JsonJavaObject();
		return jo;
	}

	/**
	 * default empty response
	 * @return
	 */
	private JsonJavaObject renderDoc() {
		JsonJavaObject jo = new JsonJavaObject();
		return jo;
	}

	/**
	 * perform a dql search
	 * @param out
	 * @param searchValue
	 */
	private void performSearch(JsonJavaObject out, String searchValue) {
		searchValue = parseSearch(searchValue);
		try {
			DQLEngine dql = new DQLEngine();
			String query = String.format(queryTemplateDocFT, searchValue, searchValue);
			
			Utils.log(query);
			Map<String, Object> dqlResult = dql.executeSearch(query);
			
			DocumentCollection col = (DocumentCollection) dqlResult.get("result");
			
			if (col != null) {
				//note the documentcollection is null if the query wasn't valid
				Utils.log("found " + col.getCount() + " documents");
				out.put("data", getResultsFromDocCol(col));
			}
			
			out.put("explain", dqlResult.get("explain"));
			out.put("count", dqlResult.get("count"));
			out.put("query", dqlResult.get("query"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private JsonJavaArray getResultsFromDocCol(DocumentCollection col) throws NotesException {
		
		Utils.log("get results from documents");
		
		JsonJavaArray arr = new JsonJavaArray();
		
		int i=0;
				
		Document doc = col.getFirstDocument();
		while (doc != null) {
			
			i++;
			if (i % 5000 == 0) {
				Utils.log("processed " + i);
			}
			
			JsonJavaObject jo = new JsonJavaObject();
			jo.put("docid", doc.getUniversalID());
			jo.put("lastname", doc.getItemValueString("lastname"));
			jo.put("firstname", doc.getItemValueString("firstname"));
			jo.put("city", doc.getItemValueString("city"));

			arr.add(jo);
			
			Document tmp = col.getNextDocument(doc);
			doc.recycle();
			doc = tmp;
		}
		
		Utils.log("got results");
		
		return arr;
	}
	
	/**
	 * if more than one search value is provided, combine them accordingly
	 * @param search
	 * @return
	 */
	private String parseSearch(String search) {
		search = search.replaceAll("\\s+", "', '");
		return "'" + search + "'";
	}

	
}
