package org.engage2020.rest;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.engage2020.Utils;
import org.engage2020.dql.JnaDqlSearch;

import com.ibm.commons.util.StringUtil;
import com.ibm.commons.util.io.json.JsonJavaObject;
import com.ibm.domino.services.ServiceException;
import com.ibm.domino.services.rest.RestServiceEngine;
import com.ibm.xsp.extlib.component.rest.CustomService;
import com.ibm.xsp.extlib.component.rest.CustomServiceBean;

/**
 * 
 * @author Mark Leusink
 *
 */
public class DQLServiceBeanJNA extends CustomServiceBean {
	
	private HttpServletRequest request;
	
	@Override
	/**
	 * this renders the response for the JSON rest service
	 */
	public void renderService(CustomService service, RestServiceEngine engine) throws ServiceException {
		
		request = engine.getHttpRequest();
		HttpServletResponse response = engine.getHttpResponse();
		response.setHeader("Content-Type", "application/json; charset=UTF-8");
		PrintWriter writer = null;
		
		long start = System.currentTimeMillis();
		
		try {
			
			//add CORS headers
			response.addHeader("Access-Control-Allow-Origin", "*");
			
			// grab the parameters
			String searchValue = request.getParameter("query");
			
			String sortBy = request.getParameter("sortBy");
			boolean sortAscending = true;
			String sortOrder = request.getParameter("sortAsc");
			if (StringUtil.isNotEmpty(sortOrder) && "false".equals(sortOrder) ) {
				sortAscending = false;
			}
			
			// parameter for paging
			int offset = 0;
			int pageSize = Integer.MAX_VALUE; // load all entries in selection
			if (StringUtil.isNotEmpty(request.getParameter("count")) ) {
				pageSize = Integer.valueOf(request.getParameter("count"));
			}
			if (StringUtil.isNotEmpty(request.getParameter("start")) ) {
				offset = Integer.valueOf(request.getParameter("start"));
			}
			
			writer = response.getWriter();
			JsonJavaObject out = new JsonJavaObject();
			
			out.put("search", searchValue);
			out.put("context", request.getPathInfo().substring(1));
			out.put("status", "ok");
			
			JnaDqlSearch.performSearch(out, searchValue, sortBy, sortAscending, offset, pageSize);
			
			writer.write(out.toString());
			
			Utils.log("Done getting sorted results with JNA in " + (System.currentTimeMillis() - start) + "ms");
			
		} catch (Exception e) {
			if (writer != null) {
				JsonJavaObject err = new JsonJavaObject();
				err.put("status", "error");
				err.put("message", e.getMessage());
				writer.write(err.toString());
				writer.close();
			}
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

}
