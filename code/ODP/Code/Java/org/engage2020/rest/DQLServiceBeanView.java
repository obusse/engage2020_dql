package org.engage2020.rest;

import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.engage2020.Utils;
import org.engage2020.helper.ConfigHelper;

import com.ibm.commons.util.io.json.JsonJavaArray;
import com.ibm.commons.util.io.json.JsonJavaObject;
import com.ibm.domino.services.ServiceException;
import com.ibm.domino.services.rest.RestServiceEngine;
import com.ibm.xsp.extlib.component.rest.CustomService;
import com.ibm.xsp.extlib.component.rest.CustomServiceBean;

import lotus.domino.Database;
import lotus.domino.DocumentCollection;
import lotus.domino.DominoQuery;
import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewEntryCollection;

/**
 * 
 * Mark Leusink
 *
 */
public class DQLServiceBeanView extends CustomServiceBean {
	
	private static String queryTemplateDocFT = "contains (%s)";
	
	private HttpServletRequest request;
	
	@Override
	/**
	 * this renders the response for the JSON rest service
	 */
	public void renderService(CustomService service, RestServiceEngine engine) throws ServiceException {
		
		request = engine.getHttpRequest();
		HttpServletResponse response = engine.getHttpResponse();
		response.setHeader("Content-Type", "application/json; charset=UTF-8");
		PrintWriter writer = null;
		
		long start = System.currentTimeMillis();
		
		try {
			
			//add CORS headers
			response.addHeader("Access-Control-Allow-Origin", "*");
			
			// grab the parameters
			String searchValue = request.getParameter("query");
			// prevent finding names with spaces
			if (searchValue.isEmpty()) {
				searchValue = "_dummy_";
			}
			
			writer = response.getWriter();
			
			JsonJavaObject out = new JsonJavaObject();
			
			out.put("search", searchValue);
			out.put("context", request.getPathInfo().substring(1));
			out.put("status", "ok");
			
			performSearch(out, searchValue);

			writer.write(out.toString());
			
			Utils.log("Done in " + (System.currentTimeMillis() - start) + "ms");
			
		} catch (Exception e) {
			if (writer != null) {
				JsonJavaObject err = new JsonJavaObject();
				err.put("status", "error");
				err.put("message", e.getMessage());
				writer.write(err.toString());
				writer.close();
			}
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	/**
	 * perform a dql search, apply the results to a view, intersect view the
	 * view and return the sorted results
	 * (in V11.0 the intersect function doesn't work for > 16.000 results)
	 * 
	 * @param out
	 * @param searchValue
	 * @throws NotesException 
	 */
	private void performSearch(JsonJavaObject out, String searchValue) throws NotesException {
		
		searchValue = parseSearch(searchValue);
			
		ConfigHelper config = new ConfigHelper();
		
		Database db = config.getSourceDatabase();
		
		// create the DQL object
		DominoQuery dQuery = db.createDominoQuery();
		
		// set a timeout on the query
		dQuery.setTimeoutSec(2);
		
		// execute the query
		String query = String.format(queryTemplateDocFT, searchValue);
		
		Utils.log(query);
		
		DocumentCollection col = dQuery.execute(query);
		
		Utils.log("found " + col.getCount() + " documents");
		
		out.put("count", col.getCount());
		out.put("query", query);
		
		// explain is a special method that returns a string explaining how the search was performed
		out.put("explain", dQuery.explain(query));
					
		JsonJavaArray sortedResults = getResultsFromView(col);
		out.put("data", sortedResults);
				
	}
	
	private JsonJavaArray getResultsFromView(DocumentCollection col) throws NotesException {
		
		Utils.log("get results from view");
		
		JsonJavaArray arr = new JsonJavaArray();
		
		View vw = col.getParent().getView("contacts");
		
		int i=0;
		
		//resort the view by lastname (ascending)
		vw.resortView("lastName", false);
		
		
		ViewEntryCollection vec = vw.getAllEntries();
		
		Utils.log("got all entries: " + vec.getCount());
		
		vec.intersect(col, true);
		
		Utils.log("intersected, vec now has " + vec.getCount());
		
		ViewEntry ve = vec.getFirstEntry();
		while (null != ve) {
			
			i++;
			if (i % 5000 == 0) {
				Utils.log("processed " + i);
			}
			
			//need to use 'isValid()' method to deal with "Entry is no longer in view" error msg
			if (ve.isValid()) {
				
				@SuppressWarnings("unchecked")
				Vector<Object> colValues = ve.getColumnValues();
				
				JsonJavaObject jo = new JsonJavaObject();
				jo.put("docid", ve.getUniversalID());
				jo.put("lastname", colValues.get(1) );
				jo.put("firstname", colValues.get(0) );
				arr.add(jo);
			
			}
			
			ViewEntry next = vec.getNextEntry();
			ve.recycle();
			ve = next;
		}
	
		Utils.log("Done reading results from view, read " + i + " view entries");
		
		return arr;
	}

	/**
	 * if more than one search value is provided, combine them accordingly
	 * @param search
	 * @return
	 */
	private String parseSearch(String search) {
		search = search.replaceAll("\\s+", "', '");
		return "'" + search + "'";
	}

}
