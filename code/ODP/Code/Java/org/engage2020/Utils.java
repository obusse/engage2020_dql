package org.engage2020;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Utils {

	private static SimpleDateFormat sf = new SimpleDateFormat("HH:mm:ss.SSS");

	/**
	 * just log to the console for debugging
	 * 
	 * @param msg
	 */
	public static void log(String msg) {
		Date now = new Date();
		System.out.println("**** ENGAGE 2020 - DQLENGINE: " + sf.format(now) + " -> " + msg);
	}

}
