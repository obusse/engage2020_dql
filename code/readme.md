# Code Documentation

## ODP (Notes project)

This is the NSF component that accesses the data and provides a JSON service.

### Calling the service

To call the DQL service use the following URL:

`http(s)://hostname.local/pathToNSF/service.xsp/<endpoint>`

#### Valid parameters

|Endpoint|Description|
|---|---|
|view|Grab all data from a view. Provide the view name as parameter `name`. Provide `count` number of lines to be returned, `start` is the number of the line to start the output with. `start` should be a multiplier of `count`, e.g. `start=201&count=100`|
|doc|Grab all data from a doc. Provide the UNID as parameter `unid`|
|search|Given the param `query` it searches in all lastname and firstname fields using `contains`|