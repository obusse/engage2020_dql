import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';

import { AgGridModule } from 'ag-grid-angular';

import { createCustomElement } from '@angular/elements';

import { ViewComponentComponent } from './view-component/view-component.component';

@NgModule({
  declarations: [
    ViewComponentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AgGridModule.withComponents([]),

  ],
  entryComponents: [
    ViewComponentComponent
  ]
  //bootstrap: [ViewComponentComponent]
})
export class AppModule {

  constructor(private injector: Injector) { }
  ngDoBootstrap() {
    const ngElement = createCustomElement(ViewComponentComponent, {
      injector: this.injector
    });
    customElements.define('engage-view-webcomponent', ngElement);
  }
}
