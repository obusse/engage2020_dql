import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { IDatasource, IGetRowsParams } from 'ag-grid-community';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-view-component',
  templateUrl: './view-component.component.html',
  styleUrls: ['./view-component.component.css']
})
export class ViewComponentComponent implements OnInit {
  @Input() endpoint: string;    // API endpoint to which the query will be send
  @ViewChild('agGrid', { static: false }) agGrid: any;

  private gridFilterSubject: Subject<string> = new Subject();
  gridState = { filter: null, sortModel: null, rowIndex: 0, unid: null, total: 0 };
  gridOptions = {};

  constructor(private http: HttpClient) {

    // column definition and grid setup
    this.gridOptions = {
      columnDefs: [
        { headerName: 'First name', field: 'firstname' },
        { headerName: 'Last name', field: 'lastname' },
        { headerName: 'Street', field: 'street' },
        { headerName: 'City', field: 'city' },
        { headerName: 'Email', field: 'email' }
      ],
      enableColResize: true,
      enableSorting: true,
      enableFilter: false,
      components: {
        loadingRenderer: function (params) {
          if (params.value !== undefined) {
            return params.value;
          } else {
            return '<img src="assets/loading.gif">';
          }
        }
      },
      rowSelection: 'single',
      rowBuffer: 0,
      rowModelType: 'infinite',
      cacheOverflowSize: 2,
      maxConcurrentDatasourceRequests: 1,
      infiniteInitialRowCount: 500,
      cacheBlockSize: 100,
      enableServerSideSorting: true
    };

  }

  ngOnInit() {

    if (!this.endpoint) {
      const defaultEndpoint = 'http://10.211.55.10/engage/api.nsf/service.xsp'
      console.warn('Missing attribute \'endpoint\' in component configuration, reverting to default (' + defaultEndpoint + ')');
      this.endpoint = defaultEndpoint;
    }

    // initialize gridFilter Subject for a debounce on entering a filter value

    this.gridFilterSubject.pipe(
      debounceTime(400)
    ).subscribe(filterValue => {
      // on filter change, we scroll to the top of the list and clear all data
      this.agGrid.api.ensureIndexVisible(0, 'top');
      this.agGrid.api.onFilterChanged();

      // update grid state
      this.gridState.rowIndex = 0;
    });

  }

  onGridReady() {

    this.agGrid.api.sizeColumnsToFit();

    // set up the datasource for the grid
    const dataSource = new ContactsDatasource(this.http, this.gridState, this.endpoint);
    this.agGrid.api.setDatasource(dataSource);

  }

  // called when a new values is entered in the filter box
  onGridFilterChanged() {
    this.gridFilterSubject.next(this.gridState.filter);
  }

  // called when the filter is 'cleared'
  resetFilter() {
    this.gridState.filter = null;
    this.onGridFilterChanged();
  }

  // called when the sort order has change (clicking on the column)
  // persist sort order, scroll to top
  onGridSortChanged() {
    this.gridState.sortModel = this.agGrid.api.getSortModel();
    this.gridState.rowIndex = 0;
    this.agGrid.api.ensureIndexVisible(0, 'top');
  }

}

export class ContactsDatasource implements IDatasource {

  constructor(private http: HttpClient,
    private gridFilter: any, private endpoint: String) {
  }

  getRows(params: IGetRowsParams): void {
    // function called when the grid asks for data
    // includes start index, number of rows,
    // sort col/ order, filter option

    const count = params.endRow - params.startRow;

    // add start/ count params
    const httpParams = {
      'start': ((params.startRow + 1) + ''),
      'count': (count + ''),
    };

    // add sort parameters
    if (params.sortModel && params.sortModel.length > 0) {
      const first = params.sortModel[0];
      httpParams['sortBy'] = first.colId;

      // firs.sort = 'asc' or 'desc'
      httpParams['sortAsc'] = first.sort === 'asc';
    }

    // add filter
    if (this.gridFilter.filter) {
      httpParams['query'] = this.gridFilter.filter;
    }

    this.http
      .get(this.endpoint + '/search-jna', {
        'params': httpParams
      })
      .subscribe(res => {
        const data = res['data'];

        let lastRow = -1;
        if (data.length < count) {
          // api returned fewer results than asked for: must be the end of the data
          // set the index of the last result
          lastRow = params.startRow + data.length;
        }

        this.gridFilter.total = res['count']

        params.successCallback(data, lastRow);
      });

  }

}

export class Contact {
  lastName: string;
  firstName: string;
  city: string;

}