import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { createCustomElement } from '@angular/elements';
import { EngagePickerComponent } from './engage-picker/engage-picker.component';

@NgModule({
  declarations: [
    EngagePickerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  entryComponents: [
    EngagePickerComponent
  ],
  providers: [],
  // bootstrap: [EngagePickerComponent]
})
export class AppModule {

  constructor(private injector: Injector) { }
  ngDoBootstrap() {
    const ngElement = createCustomElement(EngagePickerComponent, {
      injector: this.injector
    });
    customElements.define('engage-webcomponent', ngElement);
  }

}
