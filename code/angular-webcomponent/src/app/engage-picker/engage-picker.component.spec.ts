import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngagePickerComponent } from './engage-picker.component';

describe('EngagePickerComponent', () => {
  let component: EngagePickerComponent;
  let fixture: ComponentFixture<EngagePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngagePickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngagePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
