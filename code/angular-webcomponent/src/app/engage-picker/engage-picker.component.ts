import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../_models/user.model';
import { fromEvent } from 'rxjs';
import {
  debounceTime,
  map,
  distinctUntilChanged,
  filter
} from 'rxjs/operators';

@Component({
  selector: 'app-engage-picker',
  templateUrl: './engage-picker.component.html',
  styleUrls: ['./engage-picker.component.css']
})
export class EngagePickerComponent implements OnInit, AfterViewInit {
  @Input() endpoint: string;    // API endpoint to which the query will be send
  @ViewChild('inputSearch', { static: false }) inputSearch: ElementRef;

  search: string;     // query entered by user

  showExplain: boolean;

  useJNA = false;
  sortDescending = false;
  sortBy = 'lastName';

  isSearching: boolean;
  searched: boolean;

  result: any;
  list: User[];   // result list

  constructor(private http: HttpClient) { }

  ngOnInit() {

    if (!this.endpoint) {
      const defaultEndpoint = 'http://10.211.55.10/engage/api.nsf/service.xsp'
      console.warn('Missing attribute \'endpoint\' in component configuration, reverting to default (' + defaultEndpoint + ')');
      this.endpoint = defaultEndpoint;
    }

  }

  ngAfterViewInit() {

    fromEvent(this.inputSearch.nativeElement, 'keyup').pipe(
      map((event: any) => event.target.value),
      filter(res => res.length > 2),
      debounceTime(500),
      distinctUntilChanged()
    ).subscribe((text: string) => {
      this.search = text;
      this.doSearch();
    });
  }

  doSearch() {

    this.isSearching = true;

    const e = (this.useJNA ? '/search-jna' : '/search');

    this.http.get(this.endpoint + e, {
      params: {
        query: this.search,
        sortBy: this.sortBy,
        sortAsc: (this.sortDescending ? 'false' : 'true')
      }
    })
      .subscribe(res => {

        this.searched = true;
        this.result = res;

        console.log('got', res['data'].length, 'results');

        // return first 50 results only
        this.list = (res['data'] as User[]).slice(0, 50);
        this.isSearching = false;
      }, () => {
        this.isSearching = false;
        this.searched = true;
      });

  }

  clear() {
    this.search = null;
    this.list = [];
    this.searched = false;
  }

}
