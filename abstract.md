# Now what can you really build with DQL and web components?

### Abstract

If you are a developer, the introduction of the Domino Query Language (DQL) is probably the most useful addition to Domino in years. DQL was introduced in Domino 10 and gives you a query language that you can use to find the data you need. It’s powerful and very fast. In Domino 11 it was extended with new features like wildcard operators and full text searching.

But what can really build with DQL?

In this session we will take a look at the new features and build something useful with them. To do that we add one other ingredient: web components. Think of them as reusable front-end components that can be injected into any web page. They allow you to combine components written with different frameworks (e.g. React, Vue, Angular). You might have heard of web components before, but since all the major browsers have now implemented the standard you can finally start using them!
