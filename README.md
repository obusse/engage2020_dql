# Engage 2020 - DQL

Session repository for all files like

- slides
- code
- demos
- abstract

# Example web component

The example web component was built using Angular. A built (compiled) version can be found in the `code/angular-webcomponent/dist` folder. The `test.html` file contains an example of how to include the web component in a webpage. Note that the *endpoint* attribute is required.