# Agenda ideas

Bullets in second level for description only

- Title sheet

- Intro Oliver
- Intro Mark

- What is DQL ? 
  - introduced in V10,
- Prerequisites
  - design catalog, updall -e stuff
  - domino 10, but 11 is better
- What do you NOT need
  - App Dev Pack
	- Nodejs
	- Proton
  - Java knowledge (if you like LS)
	- LS knowledge (if you like Java)
	- Javascript knowledge 
- New in DQL Domino 11
		-  No more catalog
		-  Wildcards: * ?  ...
		-  ???

- no views, no ft needed (is that true for wildcard searches??)

- Demo: DQL Explorer? (or just mention it)
- Demo
  - Show java code to execute DQL, output to console
- XPage demo
  - input, button + repeat
- API demo
  - backend: custom service bean that generates JSON
  - using ExtLib controls? probably easiest for a demo


- Intro to web components
  - custom elements, modules, html template, shadow dom
  - view component
  - form component
  - vanlla JS or use a framework (polymer, svelte, stencil.js, LWC by SalesForce)
  - Or... Angular Elements
- Intro to Angular
  - basics only

- JSON services (paging, ft search)
  - DQL queries
- combining it all together
  - real world example?
  - idea: name picker: input field, search button, list of names

- WHY?
  - performance
  - easy to use
  
- Q &amp; A



	• What do you NOT Need
	
	
	• Demo
		○ Show java code to execute DQL, output to console
		○ Create XPage + button + repeat+ field, bind to same code
		○ Create REST API (extlib controls?) to show output to JSON/ API
	• What are web components
		○ Very basic introduction
		○ Vanilla JS 
		


Resources

App Dev Pack documenation
https://doc.cwpcollaboration.com/appdevpack/docs/en/domino-query-language.html

John Curtis video series
http://www.c3ug.ca/c3ug-blog/2020/1/15/c3ug-video-dql-for-domino-v11-and-forward-with-john-curtis

https://paulswithers.github.io/blog/2020/01/21/dql-what-is-it-good-for