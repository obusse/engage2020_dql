This is where presos go.

Topic ideas go to [_agenda.md](_agenda.md)

Presos could be created with MARP, the markdown presentation editor: [https://marp.app/](https://marp.app/)

It's best to use the VS Code plugin for Marp which comes also with a preview.

To export the slides to a pdf, use this:

`sudo npm install -g @marp-team/marp-cli`

and then, being in this folder, export it with this:

`marp --pdf slidedeck.md --allow-local-files`

https://github.com/marp-team/marp-cli