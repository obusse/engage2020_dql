---
marp: true
theme: base-theme
footer: 'Engage 2020 - Now what can you really build with DQL and web components? (De09)'
---

<!-- global styling -->
<style>
    h1, h2, h3, h4 {
        /* engage orange */
        color: #FF7F01;
        /*text-shadow: 1px 1px 4px #aaa; <- does not work in PDF */
    }

    h1 {
        font-size: 1.4em;
        text-align: center;
        padding: 0px 200px 0px 200px
    }

    h2 {
        font-size:1.2em;
    }

    h6 {
        font-size: 0.8em;
    }

    section {
        background-image: url(images/EngageLogo_Black.png);background-repeat: no-repeat;
        background-position: top 10px right 10px;
        background-size: 200px
    }

    section footer {
      font-size: 0.5em;
      color: #aaa;
      margin-left: 330px
    }

    section.table-flat table {
        border: none;
        background: none;
        width: 100%;
    }

    section.table-flat table tr {
        border: none;
        background: none;
    }

    section.table-flat table td, section.table-flat table th {
        padding: 0px;
        background: transparent;
        text-align: center;
        border: none;
        width: 400px;
    }
    section.table-flat table td {
        font-size: 0.8em;
    }

    code {
        background-color: #ddd;
    }

    pre {
        font-size: 0.8em;
        background-color: #eee;
    }
</style>

# Now what can you really build with DQL and web components?

<!-- _class: table-flat -->
<!-- paginate: false -->

|Mark Leusink||Oliver Busse|
|---|---|---|
|![height:200px](images/mark.jpg)|![height:200px](images/hcl-domino.png)|![height:200px](images/oliver.jpg)|
|Viaware (NL)|#dominoforever|We4IT (D)|
|@markleusink||@zeromancer1972|
|![](images/ibmchampion.jpg)||![](images/ibmchampion.jpg)|
|||![height:40px](images/hclmaster.png)|

<!-- short intro to ourselves -->

---

## Disclaimer

<!-- paginate: true -->
<!-- refer to webcasts by John Curtis (see resources), good for syntax intro. But what next? How do you actually use DQL? -->

![bg 50%](images/bong.png)

> We do ***not*** cover the general syntax and purpose of DQL in this session. We are covering the new features introduced in Domino 11 combined with a web app that uses web components and a RESTful backend to provide some fresh ideas for your upcoming development.

> The aim of this session is simply to make DQL (a lot) **less abstract**.

---

## Agenda

- What is DQL ?
- Prerequisites
- What you do **NOT** need
- Simple Example
- New in DQL in Domino 11
- Demo
  - XPages, pure API
- Intro to Web Components &amp; Angular
- Q &amp; A

<!-- shouldn't be more than 6-8 bullets -->

---

## What is DQL?

> Domino Query Language (DQL) is a facility running in Domino ***core*** allowing for a terse shorthand syntax for finding documents according to a wide variety of complexity of terms. It leverages existing design elements without the need to write detailed code to access them.

###### Source: https://doc.cwpcollaboration.com/appdevpack/docs/en/domino-query-language.html

---

## What is DQL? (in our own words)

> Domino Query Language is a great new feature of Domino 10+ that allows developers to offer a new and versatile way to search for data in even huge environments. It goes way beyond full text search and has an SQL-like syntax that is *relatively* easy to learn and easy to code.

###### Source: those two guys in front of the room

---

## What is DQL? (more figures)

- It is ***NOT*** using Elastic Search
  - is this still a thing, HCL?
- It is an entirely new developed part of Domino
  - **John Curtis** (@john_d_curtis_) is the head of development

---

## What do you need to use DQL?

- a HCL Notes/Domino 10/11 (for the new stuff)
- That's it. Really.

DQL is evolving fast, so if you're on V10, upgrade to V11 if you can. You have to if you want to use the full text/ wildcard queries anyway.

---

## What you do ***NOT*** need

- AppDev pack
- Proton (part of the AppDev pack)
- Node.js (@domino/domino-db needs Proton, yes)
  - however, you can utilize DQL in Node apps, too
- Java knowledge (if you use LotusScript)
- LotusScript knowledge (if you use Java)
- JavaScript knowledge (unless you use Node)
- Formula language

---

## Getting ready for DQL

<!--Takeaway: Use any language you prefer. Or any frontend client/ technology.

Design catalog in v10 was a separate database (GQFdsgn.cat)

Design catalog needs updating if you change the design of the app-->

- Create/Update the so called "Design Catalog" for a database
  - `load updall <database path> -e` to ***e***nable
  - `load updall <database path> -d` to up***d***ate
- or for ***all*** (suitable) databases (lazy but maybe not recommended)
  - `load updall -e`
  - `load updall -d`

This will create 'hidden' documents in the database that are used by DQL to optimize the queries.

---

## Getting ready for DQL

<!--
catalog needs to be updated when design changes,
so DQL know what it can use to fulfill the queries
-->

- Design catalog can be updated programmatically from LS/ Java:
  - `setRebuildDesignCatalog()`
  - `setRefreshDesignCatalog() `
- Read current using:
  - `listIndexes()`

(methods of the `DominoQuery` class)

---

## Simple Example in LotusScript

<!--Notice the issue here: what if your query returned 100,000 results?

Same as in Java-->

```vbscript
' hello, LotusScript :-)
Sub Initialize
    Dim session As New NotesSession
    Dim db As NotesDatabase
    Dim dql As NotesDominoQuery
    Dim col As NotesDocumentCollection
    Dim doc As NotesDocument

    Set db = session.Currentdatabase
    Set dql = db.Createdominoquery()

    Set col = dql.Execute("<your query goes here>")
    Set doc = col.Getfirstdocument()

    Do While Not doc Is Nothing
        ' do whatever you want
        Set doc = col.Getnextdocument(doc)
    Loop
End Sub
```

---

## Simple Example (cont'd)

- It's similar to building:
  - a NotesDocumentCollection (via db.Search or db.FTSearch)
  - a NotesNoteCollection (via db.CreateNoteCollection)

- Tip: DQL does not work on remote databases:
 
+![height:200px](images/remote.png)

<!--
Remote: you can't start an agent in the client and perform a query on a server based database.
-->

---

## New in DQL in Domino 11

<!-- this is the fun part... --> 

- **CONTAINS** operator
  `lastName contains all ('busse', 'leusink')`
  (search for field value)

  `contains ('mark', 'oliver')` 
  (search for documents)

DQL is picky: `contains('mark', 'oliver')` won't work.
(spot the difference...) 

<!--
be aware of syntax issues: can bite you-->

---

## New in DQL in Domino 11 (cont'd)

- wildcards
`name contains all('bu??e', 'leus*')`

- **requires FT index** (throws error if not present)
- has a cool new option to update the index right ***before*** performing the query:
  - `setRefreshFullText()`
- beware searching your own name in a huge dataset (`$UpdatedBy` 😉)

---

## Demo

- REST API based on ExtLib controls 
  - Go to **'Six Polite Ways to Design a RESTful API for Your Application!'** for other options to build an API (Wednesday, 8 AM).

<!--
- show API output
- doccol vs view needed? or show only view output
-->

---

### Sorting DQL results

- DQL queries return a `DocumentCollection` that is not sorted.
- Can be sorted using `intersect(col, maintainOrder)` method
  - Added in v10
  - If the method actually worked (which doesn't do well): there's an issue with collections > 16,000 documents.

---

### Sorting DQL results (cont'd)

<!--Users always (well mostly) want sorting

might not be in your scenario-->

- Sorting IMHO is essential
  - So is paging
- What alternative do we have?
  - Sorting in the client (requires full dataset) - might work for you
  - Domino JNA (https://github.com/klehmann/domino-jna) 


---

### Something beyond: Domino JNA

<!--Relevant because JNA allows you to do sorting with DQL-->

- An open source project by **Karsten Lehmann** that surfaces some of the 'hidden' C APIs in Java.
- JNA allows you to get dynamically sorted DQL search results.
- Has a lot of other useful features.
- Tip: disable this for (way!) better performance for DQL/ view operations:
![height:250px](images/view-props.png)

<!-- I heard rumours today about 'Domino JNX' -->

---

### So how are results sorted?

This helps in understanding what's going on:

- A DQL result is a DocumentCollection, which is basically a list of Note ID's
- A view index can be thought of as a table with rows.
- Depending on the design of the view, the table can be sorted.
- You could read just the rows from the view (in order) that match any of the ID's in the list/ DocumentCollection.

Demo

---

## But why web components?

<!--
- So we can now use DQL (and JNA) to build an API that can get sorted results
- Now how are we going to use this? And why not simply in XPages?
-->

- Suppose you run and maintain a large XPage app
- But you find it hard to find XPage developers
  - (or you think that XPage doesn't really have a bright future)
- Then web components offer a way out
  - You can load web components in an (existing) XPage application
  - And gradually rewrite your application in a modern framework
  (this is actually what we're doing in my company at the moment...)


---

## Intro to web components (1/3)

<!--
- Custom elements: create you own custom HTML tags
- Shadow DOM: independent from 'regular' dom
- HTML templates: snippets of HTML that can act as a template
- ES Modules: load dependencies

ES modules, previously called HTML Imports
Supported by all major browsers. Except if you have to support v11.
-->

Web components is a set of standards that allow you to create reusable web components:
- Custom Elements
- Shadow DOM
- HTML Templates
- ~~HTML Imports~~ ES Modules

---

## Intro to web components (2/3)

Basically:
- you write your component
- package it as a web component: JS & CSS file to be loaded on your page
- and in a web page you can then do:
`<my-component></my-component>`

---
 
## Intro to web components (3/3)

- Web components is not a framework,  but you can use one make writing them easier.
- Frameworks (not exhaustive):
  - Polymer
  - Svelte
  - React
  - Angular (I know that!)
- Web components allow you to mix-and-match components (Angular + React + Vue + XPages).

---

## We picked Angular

<!--But why?

- Can describe our own use case (we're using Angular components in a large XPage app to slowly move away from XPages)

-->

- One of the frontend frameworks to write applications in.
- Just like React, Vue.js, Svelte, Aurelia, Ember, Knockout, Mithril, Knockout, Spine, Neo and hundreds of others
- We chose Angular because... well that's what I know
- Pro: a typical Angular app is already component based and Angular allows you to 'export' a component as a web component.
---

## Demo app

- Name picker. Because every business app has one.
- Written as an Angular component.
- Wrapped as a web component ('custom element')
- Run in a basic web page (or XPage).
- Calls API running in Domino 11.
- API uses DQL to query a database (100,000 contact docs)

---

## Creating the demo component

- Initialize a new Angular app: `ng new engage-webcomponent`
- add dependency: `npm install --save @angular/elements`
- update `app.module.ts`: remove default 'bootstrap' code, define custom element
- write the component
- build the component with `ng build --prod --output-hashing=none`

Demo

<!--
Demo JNA picker & also show view component as a bonus

DQL currently only useful with JNA-->>

---

## Q &amp; A

![bg 50%](images/qa.png)

---

![bg 70%](images/thanks.gif)

---

## Resources - DQL

- https://gitlab.com/obusse/engage2020_dql
- https://doc.cwpcollaboration.com/appdevpack/docs/en/domino-query-language.html
- https://oliverbusse.notesx.net/hp.nsf/tags.xsp?query=dql
- https://help.hcltechsw.com/dom_designer/designer_welcome.html
- http://www.c3ug.ca/c3ug-blog/2020/1/15/c3ug-video-dql-for-domino-v11-and-forward-with-john-curtis
- https://www.eknori.de/2019-06-06/get-sorted-results-from-dql-queries/
- https://github.com/klehmann/domino-jna

--- 

## Resources - Web components/ Angular

- https://www.webcomponents.org/introduction
- https://www.techiediaries.com/angular/angular-9-elements-web-components/
